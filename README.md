- run command `yarn` or `npm install`
- after that, run `yarn start` to start development mode
- this application should be run at `http://localhost:8000`

Demo: https://test-codefood.web.app \
Figma design: https://www.figma.com/file/OlTAO4UlS3QOquAUXCL4XM/Devcode-Challenges?node-id=219%3A1177 \
Base API: https://fe.runner.api.devcode.biofarma.co.id \
Postman docs: https://documenter.getpostman.com/view/3235139/UVeGpR9V \

Luqni Maulana\
email: `maulana.7jr@gmail.com`