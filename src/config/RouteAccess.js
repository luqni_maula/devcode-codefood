import Config from '@config/Config';

const {BASE_API} = Config;

const RouteAccess = {
    getApi: function(name, object) {
        if (this.hasOwnProperty(name)) {
        	let url = this[name];
        	for (let key in (object ?? {})) {
        		url = url.replace(`{{${key}}}`, object[key]);
        	}
        	return url;
        }
        return null;
    },
    AUTH_SIGNIN: BASE_API + '/auth/login',
    RECIPES: BASE_API + '/recipes',
    RECIPES_SUGGEST: BASE_API + '/search/recipes',
    RECIPES_DETAIL: BASE_API + '/recipes/{{id}}',
    RECIPES_CATEGORIES: BASE_API + '/recipe-categories',
    RECIPES_STEPS: BASE_API + '/recipes/{{id}}/steps',
    START_COOKING: BASE_API + '/serve-histories',
    DETAIL_COOKING: BASE_API + '/serve-histories/{{id}}',
    COOKING_STEP_DONE: BASE_API + '/serve-histories/{{id}}/done-step',
    COOKING_REACTION: BASE_API + '/serve-histories/{{id}}/reaction',
    COOKING_HISTORY: BASE_API + '/serve-histories',
};

export default RouteAccess;
