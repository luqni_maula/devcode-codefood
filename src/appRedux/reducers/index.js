import {combineReducers} from "redux";
import {connectRouter} from 'connected-react-router';
import Common from "./Common";
import Recipes from "./Recipes";
import Auth from "./Auth";
import Cooking from "./Cooking";
import History from "./History"; 

export default (history) => combineReducers({
    router: connectRouter(history),
    common: Common,
    recipes: Recipes,
    auth: Auth,
    cooking: Cooking,
    history: History
});