import {
	HISTORY_LOADING,
	HISTORY_DATA,
	HISTORY_TOTAL,
	HISTORY_LIMIT,
	HISTORY_PAGE,
	HISTORY_CATEGORY,
	HISTORY_SORTER,
	HISTORY_STATUS,
	HISTORY_SEARCH
} from "@constants/ActionTypes";

const sorterOptions = [
	{
		name: 'Terbaru',
		value: null,
		cy: 'button-sort-latest'
	},
	{
		name: 'Terlama',
		value: 'oldest',
		cy: 'button-sort-oldest'
	},
	{
		name: 'Porsi Terbanyak',
		value: 'nserve_desc',
		cy: 'button-sort-most'
	},
	{
		name: 'Porsi Tersedikit',
		value: 'nserve_asc',
		cy: 'button-sort-least'
	},
];

const INIT_STATE = {
	loading: false,
	data: [],
	total: 0,
	limit: 10,
	page: 1,
	category: null,
	sorters: sorterOptions,
	sorter: null,
	status: null,
	search: ''
};

export default (state = INIT_STATE, action) =>
{
    switch (action.type)
    {
        case HISTORY_LOADING: {
            return {...state, loading: action.payload ? true : false};
        }

        case HISTORY_DATA: {
            return {...state, data: action.payload ?? INIT_STATE.data};
        }

        case HISTORY_TOTAL: {
            return {...state, total: action.payload ?? INIT_STATE.total};
        }

        case HISTORY_LIMIT: {
            return {...state, limit: action.payload ?? INIT_STATE.limit};
        }

        case HISTORY_PAGE: {
            return {...state, page: action.payload ?? INIT_STATE.page};
        }

        case HISTORY_CATEGORY: {
            return {...state, category: action.payload};
        }

        case HISTORY_SORTER: {
            return {...state, sorter: action.payload ?? INIT_STATE.sorter};
        }

        case HISTORY_STATUS: {
            return {...state, status: action.payload ?? INIT_STATE.status};
        }

        case HISTORY_SEARCH: {
            return {...state, search: action.payload ?? INIT_STATE.search};
        }

        default:
            return state;
    }
}