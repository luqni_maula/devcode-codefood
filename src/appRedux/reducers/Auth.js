import {
	AUTH_LOADING,
	AUTH_TOKEN,
	AUTH_ERROR_MESSAGE
} from "@constants/ActionTypes";

const INIT_STATE = {
    loading: false,
    token: localStorage.getItem('token') || null,
    error_message: ''
};

export default (state = INIT_STATE, action) =>
{
    switch (action.type)
    {
        case AUTH_LOADING: {
            return {...state, loading: action.payload ? true : false};
        }

        case AUTH_TOKEN: {
        	if (action.payload) {
        		localStorage.setItem('token', action.payload);
        	} else {
        		localStorage.removeItem('token');
        	}
            return {...state, token: action.payload ?? null};
        }

        case AUTH_ERROR_MESSAGE: {
            return {...state, error_message: action.payload ?? ''};
        }

        default:
            return state;
    }
}