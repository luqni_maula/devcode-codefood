import {
	RECIPE_LOADING,
	RECIPE_SEARCH,
	RECIPE_DATA,
	RECIPE_TOTAL,
	RECIPE_PAGE,
	RECIPE_LIMIT,
	RECIPE_CATEGORIES_LOADING,
	RECIPE_CATEGORIES,
	RECIPE_SELECTED_CATEGORY,
	RECIPE_SORTERS,
	RECIPE_SELECTED_SORTER,
} from "@constants/ActionTypes";
import {sorterOptions} from "@constants/General";

const INIT_STATE = {
    loading: false,
    search: '',
	data: [],
    total: 0,
	page: 1,
	limit: 12,
	categories_loading: false,
	categories: [],
	selected_category: null,
	sorters: sorterOptions,
	selected_sorter: null,
};

export default (state = INIT_STATE, action) =>
{
    switch (action.type)
    {
        case RECIPE_LOADING: {
            return {...state, loading: action.payload ? true : false};
        }

        case RECIPE_SEARCH: {
            return {...state, search: action.payload || ''};
        }

        case RECIPE_DATA: {
            return {...state, data: action.payload ? action.payload : []};
        }

        case RECIPE_TOTAL: {
            return {...state, total: action.payload || 0};
        }

        case RECIPE_PAGE: {
            return {...state, page: action.payload || 1};
        }
        
        case RECIPE_LIMIT: {
            return {...state, limit: action.payload || 12};
        }

        case RECIPE_CATEGORIES_LOADING: {
        	return {...state, categories_loading: action.payload ? true : false};
        }

		case RECIPE_CATEGORIES: {
			return {...state, categories: action.payload || []};
		}

		case RECIPE_SELECTED_CATEGORY: {
			return {...state, selected_category: action.payload || null};
		}

		case RECIPE_SORTERS: {
			return {...state, sorters: action.payload || []};
		}

		case RECIPE_SELECTED_SORTER: {
			return {...state, selected_sorter: action.payload || null};
		}


        default:
            return state;
    }
}