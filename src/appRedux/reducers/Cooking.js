import {
	COOKING_LOADING,
	COOKING_DATA,
	COOKING_STEPS,
} from "@constants/ActionTypes";

const INIT_STATE = {
	loading: false,
	data: null,
	steps: []
};

export default (state = INIT_STATE, action) =>
{
    switch (action.type)
    {
        case COOKING_LOADING: {
            return {...state, loading: action.payload ? true : false};
        }

        case COOKING_DATA: {
            return {...state, data: action.payload || null};
        }

        case COOKING_STEPS: {
            return {...state, steps: action.payload ?? []};
        }

        default:
            return state;
    }
}