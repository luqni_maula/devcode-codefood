import {
	AUTH_LOADING,
	AUTH_TOKEN,
	AUTH_ERROR_MESSAGE
} from "@constants/ActionTypes";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError} from "@reduxActions/Common";

export const signIn = (username, password, callback) =>
{
	return async (dispatch) =>
	{
		try {
			dispatch({type: AUTH_LOADING, payload: true});
			const {data: {success, message, data}} = await Api.post(RouteAccess.getApi('AUTH_SIGNIN'), {username, password});
			if (success) {
				dispatch({type: AUTH_TOKEN, payload: data.token});
				if (callback) callback(true);
			} else {
				dispatch({type: AUTH_ERROR_MESSAGE, payload: message});
				// dispatch(fetchError(message));
			}
		} catch(error) {
			dispatch(setError(error));
		} finally {
			dispatch({type: AUTH_LOADING, payload: false});
		}
	}
}

export const setErrorMessage = (value) =>
{
	return {type: AUTH_ERROR_MESSAGE, payload: value};
}

export const resetErrorMessage = () =>
{
	return {type: AUTH_ERROR_MESSAGE, payload: ''};
}