import {
	RECIPE_LOADING,
	RECIPE_SEARCH,
	RECIPE_DATA,
	RECIPE_TOTAL,
	// RECIPE_PAGE,
	// RECIPE_LIMIT,
	RECIPE_CATEGORIES_LOADING,
	RECIPE_CATEGORIES,
	RECIPE_SELECTED_CATEGORY,
	// RECIPE_SORTERS,
	RECIPE_SELECTED_SORTER,
} from "@constants/ActionTypes";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError} from "@reduxActions/Common";

export const getRecipes = () =>
{
	return async (dispatch, getState) =>
	{
		const {recipes: {selected_sorter, selected_category, search}} = getState();

		try {
			dispatch({type: RECIPE_LOADING, payload: true});

			let params = {};
			if (selected_sorter) params['sort'] = selected_sorter;
			if (selected_category) params['categoryId'] = selected_category;
			if (search) params['q'] = search;

			const {data} = await Api.get(RouteAccess.getApi('RECIPES'), {params});

			dispatch({type: RECIPE_DATA, payload: data?.data?.recipes});
			dispatch({type: RECIPE_TOTAL, payload: data?.data?.total});
		} catch(error) {
			dispatch(setError(error));
		} finally {
			dispatch({type: RECIPE_LOADING, payload: false});
		}
	}
}

export const getRecipesCategories = () =>
{
	return async (dispatch, getState) =>
	{
		try {
			dispatch({type: RECIPE_CATEGORIES_LOADING, payload: true});
			const {data} = await Api.get(RouteAccess.getApi('RECIPES_CATEGORIES'));
			dispatch({type: RECIPE_CATEGORIES, payload: data?.data});
		} catch(error) {
			dispatch(setError(error));
		} finally {
			dispatch({type: RECIPE_CATEGORIES_LOADING, payload: false});
		}
	}
}

export const setRecipesCategory = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: RECIPE_SELECTED_SORTER, payload: null});
		dispatch({type: RECIPE_SELECTED_CATEGORY, payload: value});
		dispatch(getRecipes());
	}
}

export const setRecipesSorter = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: RECIPE_SELECTED_SORTER, payload: value});
		dispatch(getRecipes());
	}
}

export const setRecipesSearch = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: RECIPE_SEARCH, payload: value});
	}
}