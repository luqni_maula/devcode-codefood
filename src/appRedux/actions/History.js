import {
	HISTORY_LOADING,
	HISTORY_DATA,
	HISTORY_TOTAL,
	// HISTORY_LIMIT,
	HISTORY_PAGE,
	HISTORY_CATEGORY,
	HISTORY_SORTER,
	HISTORY_STATUS,
	HISTORY_SEARCH
} from "@constants/ActionTypes";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError} from "@reduxActions/Common";

export const getHistory = () =>
{
	return async (dispatch, getState) =>
	{
		const {limit, page, data, status, category, sorter, search} = getState().history;
		try {
			dispatch({type: HISTORY_LOADING, payload: true});
			let params = {limit};
			if (page > 1) params['skip'] = data.length;
			if (status) params['status'] = status;
			if (sorter) params['sort'] = sorter;
			if (category) params['categoryId'] = category;
			if (search) params['q'] = search;
			const {data: {data: {history, total}}} = await Api.get(RouteAccess.getApi('COOKING_HISTORY'), {params});
			dispatch({type: HISTORY_DATA, payload: [...data, ...history]});
			dispatch({type: HISTORY_TOTAL, payload: total});
		} catch(e) {
			dispatch(setError(e));
		} finally {
			dispatch({type: HISTORY_LOADING, payload: false});
		}
	}
}

export const resetHistory = () =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_DATA, payload: []});
		dispatch({type: HISTORY_TOTAL, payload: 0});
		dispatch({type: HISTORY_PAGE, payload: 1});
	}
}

export const setHistoryCategory = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_PAGE, payload: 1});
		dispatch({type: HISTORY_DATA, payload: []});
		dispatch({type: HISTORY_CATEGORY, payload: value});
		dispatch({type: HISTORY_STATUS, payload: null});
		dispatch(getHistory());
	}
}

export const setHistoryStatus = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_PAGE, payload: 1});
		dispatch({type: HISTORY_DATA, payload: []});
		dispatch({type: HISTORY_CATEGORY, payload: value ? undefined : null});
		dispatch({type: HISTORY_STATUS, payload: value});
		dispatch(getHistory());
	}
}

export const setHistorySorter = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_PAGE, payload: 1});
		dispatch({type: HISTORY_DATA, payload: []});
		dispatch({type: HISTORY_SORTER, payload: value});
		dispatch(getHistory());
	}
}

export const setHistorySearch = (value) =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_SEARCH, payload: value});
	}
}

export const resetHistoryData = () =>
{
	return (dispatch) =>
	{
		dispatch({type: HISTORY_PAGE, payload: 1});
		dispatch({type: HISTORY_DATA, payload: []});
	}
}

export const loadMoreHistory = () =>
{
	return (dispatch, getState) =>
	{
		const {loading, page, limit, total} = getState().history;

		if (!loading && (page * limit) < total)
		{
			dispatch({type: HISTORY_PAGE, payload: page + 1});
			dispatch(getHistory());
		}
	}
}