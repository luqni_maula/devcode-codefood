import {
	COOKING_LOADING,
	COOKING_DATA,
	COOKING_STEPS,
} from "@constants/ActionTypes";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError, fetchError} from "@reduxActions/Common";
import {history} from "@reduxStore";

export const getCookingSteps = (id, callback) =>
{
	return async (dispatch, getState) =>
	{
		try {
			dispatch({type: COOKING_LOADING, payload: true});
			const {data: {success, message, data}} = await Api.get(RouteAccess.getApi('RECIPES_STEPS', {id}));
			if (success) {
				dispatch(setCookingData(null, data));
			} else {
				dispatch(fetchError(message));
			}
		} catch(e) {
			dispatch(setError(e));
		} finally {
			dispatch({type: COOKING_LOADING, payload: false});
			if (callback) callback();
		}
	}
}

export const getCookingData = (id) =>
{
	return async (dispatch) =>
	{
		try {
			dispatch({type: COOKING_LOADING, payload: true});
			const {data: {success, message, data}} = await Api.get(RouteAccess.getApi('DETAIL_COOKING', {id}));
			if (success) {
				dispatch(setCookingData(data, data?.steps));
			} else {
				dispatch(fetchError(message));
			}
		} catch(e) {
			dispatch(setError(e));
		} finally {
			dispatch({type: COOKING_LOADING, payload: false});
		}
	}
}

export const setCookingData = (data, steps) =>
{
	return (dispatch) =>
	{
		dispatch({type: COOKING_DATA, payload: data});
		dispatch({type: COOKING_STEPS, payload: steps});
	}
}

export const markStepDone = (id, stepOrder, callback) =>
{
	return async (dispatch, getState) =>
	{
		const {router: {location}} = getState();
		try {
			if (stepOrder === 1)
			{
				const {data: {success, data, message}} = await Api.post(RouteAccess.getApi('START_COOKING'), {
					nServing: !isNaN(parseInt(location?.query?.n)) ? location?.query?.n : 1,
					recipeId: id
				});

				if (success) {
					dispatch(setCookingData(data, data?.steps));
					history.push('/cooking/' + data.id);
				} else {
					dispatch(fetchError(message));
				}
			} else {
				const {data: {success, message, data}} = await Api.put(RouteAccess.getApi('COOKING_STEP_DONE', {id}), {stepOrder});

				if (success) {
					dispatch(setCookingData(data, data?.steps));
				} else {
					dispatch(fetchError(message));
				}
			}
		} catch(e) {
			dispatch(setError(e));
		} finally {
			if (callback) callback();
		}
	}
}

export const saveReaction = (id, reaction, callback) =>
{
	return async (dispatch) =>
	{
		try {
			const {data: {success, message, data}} = await Api.post(RouteAccess.getApi('COOKING_REACTION', {id}), {reaction});

			if (success) {
				dispatch(setCookingData(data, data?.steps));
			} else {
				dispatch(fetchError(message));
			}
		} catch(e) {
			dispatch(setError(e));
		} finally {
			if (callback) callback();
		}
	}
}