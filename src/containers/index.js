import React, {memo, useState, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {useHistory, Link} from "react-router-dom";
import {setRecipesCategory, setRecipesSearch, getRecipesCategories, getRecipes} from "@reduxActions/Recipes";
import {setHistoryCategory, setHistoryStatus, setHistorySearch, getHistory, resetHistoryData} from "@reduxActions/History";
import InfoView from "@app/components/InfoView";
import Routes from "@routes";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";

let _debounce = null;

const SearchBox = memo(({isHistory, pathname}) =>
{
	const dispatch = useDispatch();
	const history = useHistory();
	const {loading, search} = useSelector(({recipes}) => recipes);
	const {loading: loadingHistory, search: searchHistory} = useSelector(({history}) => history);
	const [suggestions, setSuggestions] = useState([]);

	const fetchSuggestions = () =>
	{
		if (!isHistory)
		{
			if (_debounce)
			{
				clearTimeout(_debounce);
				_debounce = null;
			}

			_debounce = setTimeout(async () => {
				try {
					if (search.length > 1) {
						const {data} = await Api.get(RouteAccess.getApi('RECIPES_SUGGEST'), {params: {q: search}});
						setSuggestions(data?.data ?? []);
					} else {
						setSuggestions([]);
					}
				} catch(e) {
					setSuggestions([]);
				}
			}, 300);
		}
	}

	useEffect(() => {
		if (!isHistory) setSuggestions([]);
	}, [isHistory]);

	useEffect(() => {
		setSuggestions([]);
	}, [pathname]);

	return (
		<React.Fragment>
			<div className="cf-position-relative cf-align-items-center">
				<input
				data-cy="header-input-search"
				type="text" 
				disabled={loading || loadingHistory}
				onChange={({target: {value}}) => {
					if (isHistory) {
						dispatch(setHistorySearch(value));
					} else {
						dispatch(setRecipesSearch(value));
					}
					fetchSuggestions();
				}}
				value={isHistory ? searchHistory : search}
				placeholder="Cari resep" 
				className="cf-searchbox cf-mr-2"
				list="search-suggestions"/>
				{(!isHistory && suggestions && suggestions.length > 0) && (
					<div className="cf-search-suggestions cf-position-absolute" data-cy="search-suggestion-container">
						{suggestions.map(({id, name}, i) => (
							<Link to={`/detail/${id}`} key={i}
							data-cy={`search-suggestion-${i}`} 
							className="cf-mb-2 cf-pointer cf-d-block"
							onClick={() => {
								dispatch(setRecipesSearch(name));
								setSuggestions([]);
							}}>
								{name}
							</Link>
						))}
					</div>
				)}
				{(!isHistory && search) && (
					<span className="cf-searchbox-reset cf-icon-circle-close cf-pointer"
					data-cy="header-button-clear"
					onClick={() => {
						setSuggestions([]);
						dispatch(setRecipesSearch(''));
						dispatch(getRecipes());
					}}/>
				)}
				{(isHistory && searchHistory) && (
					<span className="cf-searchbox-reset cf-icon-circle-close cf-pointer"
					data-cy="header-button-clear"
					onClick={() => {
						setSuggestions([]);
						dispatch(setHistorySearch(''));
						dispatch(resetHistoryData());
						dispatch(getHistory());
					}}/>
				)}
				{isHistory ? (
					<button
					data-cy="header-button-search"
					disabled={loadingHistory}
					className="cf-btn cf-btn-primary"
					onClick={() => {
						if (!loadingHistory) {
							setSuggestions([]);
							dispatch(resetHistoryData());
							dispatch(getHistory());
						}
					}}>
						Cari
					</button>
				) : (
					<button
					data-cy="header-button-search"
					disabled={loading}
					className="cf-btn cf-btn-primary"
					onClick={() => {
						if (!loading) {
							setSuggestions([]);
							dispatch(getRecipes());
							history.push('/');
						}
					}}>
						Cari
					</button>
				)}
			</div>
		</React.Fragment>
	)
});

const AppHeader = memo(({showTabs, pathname}) =>
{
	const [isHistory, setIsHistory] = useState(false);
	const [isLogin, setIsLogin] = useState(false);

	useEffect(() => {
		setIsHistory(pathname === '/history');
		setIsLogin(pathname === '/login');
	}, [pathname]);

	return (
		<div className={`cf-header ${isHistory && 'cf-header-history'}`}>
			<div className="cf-wrapper-container">
				{isHistory && (
					<div className="cf-mb-4 cf-d-inline-flex cf-justify-content-between cf-align-items-center">
						<Link to="/" data-cy="button-back"
						className="cf-btn cf-btn-default cf-bg-white cf-mr-3 cf-align-items-center">
							<span className="cf-icon-back"/>
						</Link>
						<h2 className="cf-my-0" data-cy="header-text-title">Riwayat</h2>
					</div>
				)}
				<div className="cf-w-100 cf-d-flex cf-justify-content-between cf-align-items-center">
					<div className="cf-d-inline-flex">
						{!isHistory && (
							<Link to="/">
								<div className="cf-codefood-logo cf-mr-3"
								data-cy="header-logo"/>
							</Link>
						)}
						{!isLogin && <SearchBox pathname={pathname} isHistory={isHistory}/>}
					</div>
					{(showTabs && !isLogin) && (
						<div>
							<Link to={'/history'}
							className="cf-btn cf-btn-default cf-bg-white cf-mr-3 cf-align-items-center cf-h-100"
							data-cy="header-button-history">
								<span className="cf-icon-file-history"/>
							</Link>
						</div>
					)}
				</div>
				{((showTabs || isHistory) && !isLogin) && (
					<div className="cf-mt-3">
						<AppCategories isHistory={isHistory}/>
					</div>
				)}
			</div>
		</div>
	)
});

const HistorySorterStatus = memo(() =>
{
	const dispatch = useDispatch();
	const {status} = useSelector(({history}) => history);
	const [expanded, setExpanded] = useState(false);
	const options = [
		{
			value: null,
			text: 'Semua Status',
			cy: 'category-button-status-all'
		},
		{
			value: 'progress',
			text: 'Dalam Proses',
			cy: 'category-button-status-progress'
		},
		{
			value: 'done',
			text: 'Selesai Dimasak',
			cy: 'category-button-status-done'
		},
	];

	const active = options.find((r) => r.value === status) ?? {};

	return (
		<div className={`${active.value ? 'cf-text-primary cf-active-tab' : 'cf-text-muted'} cf-mr-4 cf-position-relative cf-pointer`}
		data-cy="category-button-status"
		onClick={() => setExpanded((v) => !v)}>
			{active.text}
			<span className={`${active.value ? 'cf-icon-arrow-down-filled' : 'cf-icon-arrow-down'} cf-ml-2 cf-text-muted cf-mt-1`}/>
			{expanded && (
				<div className="cf-history-sorter-status cf-position-absolute cf-p-3">
					{options.map(({value, text, cy}, index) => (
						<div key={index} data-cy={cy}
						className={`${index !== (options.length - 1) && 'cf-mb-3'} cf-pointer`}
						onClick={() => dispatch(setHistoryStatus(value))}>
							{text}
						</div>
					))}
				</div>
			)}
		</div>
	)
})

const AppCategories = memo(({isHistory}) => 
{
	const dispatch = useDispatch();
	const {loading, selected_category, categories} = useSelector(({recipes}) => recipes);
	const {loading: loadingHistory, category} = useSelector(({history}) => history);
	const [categoryOptions, setCategoryOptions] = useState([]);

	useEffect(() => {
		const currentCategories = [...categories];
		currentCategories.unshift({id: null, name: 'Semua'});
		setCategoryOptions(currentCategories);
	}, [categories]);

	useEffect(() => {
		dispatch(getRecipesCategories());
	}, [dispatch]);

	return (
		<div className="cf-d-inline-flex cf-justify-content-between">
			{isHistory ? (
				<React.Fragment>
					{categoryOptions.map(({id, name}, index) => (
						<div key={index} 
						data-cy={`category-button-${index}`}
						className={`${id === category ? 'cf-text-primary cf-active-tab' : 'cf-text-muted'} cf-mr-4 ${(!loadingHistory && id !== category) && 'cf-pointer'}`}
						onClick={() => {
							if (!loadingHistory && id !== category) dispatch(setHistoryCategory(id));
						}}>
							{name}
						</div>
					))}
					<HistorySorterStatus/>
				</React.Fragment>
			) : (
				<React.Fragment>
					{categoryOptions.map(({id, name}, index) => (
						<div key={index} 
						data-cy={`category-button-${index}`}
						className={`${id === selected_category ? 'cf-text-primary cf-active-tab' : 'cf-text-muted'} cf-mr-4 ${(!loading && id !== selected_category) && 'cf-pointer'}`}
						onClick={() => {
							if (!loading && id !== selected_category) dispatch(setRecipesCategory(id));
						}}>
							{name}
						</div>
					))}
				</React.Fragment>
			)}
		</div>
	)
});

export default memo(({match}) =>
{
	const {location: {pathname}} = useHistory();
	const [isLogin, setIsLogin] = useState(false);

	useEffect(() => {
		setIsLogin(pathname === '/login');
	}, [pathname]);

	return (
		<div className="cf-w-100">
			<AppHeader pathname={pathname} showTabs={pathname === '/'}/>
			<div className={!isLogin && `cf-wrapper-container`}>
				<Routes match={match}/>
			</div>
			<InfoView/>
        </div>
	)
});