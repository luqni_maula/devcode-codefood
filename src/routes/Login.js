import React, {memo, useState, useEffect} from "react";
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {signIn, setErrorMessage, resetErrorMessage} from "@reduxActions/Auth";

export default memo(() =>
{
	const dispatch = useDispatch();
	const history = useHistory();
	const {loading, error_message, token} = useSelector(({auth}) => auth);
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const handleSubmit = (e) =>
	{
		e.preventDefault();
		if (username && password) {
			if (password.trim().length < 6) {
				dispatch(setErrorMessage('Password minimal 6 karakter'));
			} else {
				dispatch(signIn(username, password, (success) => {
					if (success) history.push('/');
				}));
			}
		} else {
			alert('Mohon isi username & password anda');
		}
	}

	useEffect(() => {
		if (token) history.push('/');
		return () => dispatch(resetErrorMessage());
	}, [dispatch, token, history]);

	return (
		<div className="cf-login-container">
			<div className="cf-d-flex cf-justify-content-between cf-align-items-center cf-mx-5 cf-py-5">
				<div className="cf-pl-5">
					<div className="cf-codefood-logo-white"
					data-cy="content-logo"/>
				</div>
				<div className="cf-py-5 cf-px-5 cf-login-form">
					<div className="cf-w-100">
						<h3 className="cf-mt-0"
						data-cy="form-text-title">
							Login
						</h3>
					</div>
					<form onSubmit={handleSubmit}>
						{error_message && (
							<div className="cf-mb-4 cf-error-message" data-cy="form-alert-container">
								<div className="cf-d-flex cf-justify-content-between cf-px-3 cf-py-3">
									<small data-cy="form-alert-text">{error_message}</small>
									<small className="cf-error-message-ok"
									data-cy="form-alert-button-ok"
									onClick={() => dispatch(resetErrorMessage())}>
										OK
									</small>
								</div>
							</div>
						)}
						<div className="cf-w-100">
							<label for="username"
							data-cy="form-text-email">
								Email
							</label>
							<input
							data-cy="form-input-email"
							className="cf-username cf-mb-3"
							type="email"
							id="username"
							required
							disabled={loading}
							value={username}
							onChange={({target}) => {
								setUsername(target.value);
							}}/>
						</div>
						<div className="cf-w-100">
							<label for="password"
							data-cy="form-text-password">
								Password
							</label>
							<input
							data-cy="form-input-password"
							className="cf-password cf-mb-3"
							type="password"
							id="password"
							required
							disabled={loading}
							value={password}
							onChange={({target}) => {
								setPassword(target.value);
							}}/>
						</div>
						<button
						className="cf-btn cf-btn-primary cf-w-100"
						type="submit"
						data-cy="form-button-login"
						disabled={loading || !(username.trim() && password.trim())}>
							{loading ? 'Memverifikasi...' : 'Login'}
						</button>
						<div className="cf-w-100 cf-text-center cf-mt-5">
							<Link to="/"
							className="cf-text-primary"
							data-cy="form-button-skip">
								<b>Lewati Login</b>
							</Link>
						</div>
					</form>
				</div>
			</div>
		</div>
	)
});