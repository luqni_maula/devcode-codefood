import React, {memo, Suspense, lazy} from "react";
import {Route, Switch} from "react-router-dom";
import FallbackContent from "@app/components/FallbackContent";

export default memo(({match}) => (
	<Suspense fallback={<FallbackContent/>}>
        <Switch>
	        <Route path={`${match.url}login`} component={lazy(() => import('./Login'))}/>
	        <Route path={`${match.url}`} component={lazy(() => import('./Menu'))}/>
        </Switch>
    </Suspense>
));