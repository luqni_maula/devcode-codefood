import React, {memo, Suspense, lazy} from "react";
import {Route, Switch} from "react-router-dom";
import FallbackContent from "@app/components/FallbackContent";

export default memo(({match}) => (
	<Suspense fallback={<FallbackContent/>}>
	    <Switch>
	        <Route exact path={`${match.url}`} component={lazy(() => import('./Menu'))}/>
	        <Route path={`${match.url}detail`} component={lazy(() => import('./Detail'))}/>
	        <Route path={`${match.url}cooking`} component={lazy(() => import('./Cooking'))}/>
	        <Route path={`${match.url}history`} component={lazy(() => import('./History'))}/>
	    </Switch>
	</Suspense>
));