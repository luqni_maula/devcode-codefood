import React, {memo, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getRecipes} from "@reduxActions/Recipes";
import MenuItem from "@components/Menu/Item";
import MenuSorter from "@components/Menu/Sorter";

export default memo(() => 
{
	const dispatch = useDispatch();
	const {loading, data} = useSelector(({recipes}) => recipes);

	useEffect(() => {
		dispatch(getRecipes());
	}, [dispatch]);

	return (
		<div className="cf-w-100 cf-mt-3">
			{(data && data.length > 0) && (
				<div className="cf-mb-3 cf-w-100">
					<MenuSorter/>
				</div>
			)}
			{loading && (
				<div className="cf-w-100 cf-text-center">
					<span>Loading...</span>
				</div>
			)}
			<div className="cf-activities">
				{data.map((row, index) => (
			        <div key={index} className="cf-p-2">
			        	<MenuItem data={row} dataCy={`list-item-${index}`}/>
				    </div>
				))}
			</div>
			{!(data && data.length) && (
				<div className="cf-w-100 cf-text-center cf-mb-5">
					<span className="cf-menu-empty" data-cy="list-image-empty"/>
					<h4 className="cf-text-muted cf-fw-normal" data-cy="list-text-empty">Oops! Resep tidak ditemukan.</h4>
				</div>
			)}
		</div>
	)
});