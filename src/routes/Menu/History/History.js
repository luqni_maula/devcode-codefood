import React, {memo, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {useHistory, Link} from "react-router-dom";
import MenuSorter from "@components/Menu/Sorter";
import {getHistory, loadMoreHistory, resetHistory} from "@reduxActions/History";

const Reaction = memo(({id, status}) =>
{
	switch (status)
	{
		case 'like':
			return (
				<div className="cf-d-inline-flex cf-text-success cf-align-items-center">
					<span className="cf-icon-reaction-success cf-mr-2"/>
					<span data-cy="history-item-text-rating">Yummy!</span>
				</div>
			)
		case 'neutral':
			return (
				<div className="cf-d-inline-flex cf-text-warning cf-align-items-center">
					<span className="cf-icon-reaction-warning cf-mr-2"/>
					<span data-cy="history-item-text-rating">Lumayan</span>
				</div>
			)
		case 'dislike':
			return (
				<div className="cf-d-inline-flex cf-text-danger cf-align-items-center">
					<span className="cf-icon-reaction-danger cf-mr-2"/>
					<span data-cy="history-item-text-rating">Kurang Suka</span>
				</div>
			)
		default:
			return (
				<Link to={`/cooking/${id}/rating`}
				className="cf-text-muted cf-pointer"
				data-cy="history-item-no-rating">
					Belum Ada Penilaian
				</Link>
			)
	}
});

const getDateFormat = (dateString) =>
{
	if (dateString)
	{
		const date = new Date(dateString);
		const month = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
		return `${date.getDate()} ${month[date.getMonth()]} ${date.getFullYear()}`;
	}
	return null;
}

const HistoryItem = memo(({data, cy}) =>
{
	const {id, recipeId, recipeName, recipeCategoryName, recipeImage, nServing, status, reaction, createdAt, nStep, nStepDone} = data;
	return (
		<div className="cf-w-100 cf-bg-white cf-rounded-lg cf-mb-3" data-cy={cy}>
			<div className="cf-p-4">
				<div className="cf-text-muted">
					<span data-cy="history-item-text-date">{getDateFormat(createdAt)}</span> - <span data-cy="history-item-text-code">{id}</span>
				</div>
				<div className="cf-d-flex cf-mt-3">
					<img alt="" src={recipeImage} draggable="false" data-cy="history-item-image"
					className="cf-rounded-lg cf-size-175 cf-object-fit-cover"/>
					<div className="cf-ml-4 cf-w-100">
						<h3 className="cf-mt-0 cf-mb-2" data-cy="history-item-text-title">{recipeName}</h3>
						<div className="cf-d-flex cf-justify-content-between">
							<h4 className="cf-mt-0 cf-fw-normal cf-text-muted" data-cy="history-item-text-category">
								{recipeCategoryName}
							</h4>
							<h4 className="cf-mt-0 cf-fw-normal" data-cy="history-item-text-portion">
								Porsi: <b>{nServing}</b>
							</h4>
						</div>
					</div>
				</div>
			</div>
			<hr className="cf-my-0"/>
			<div className="cf-p-4 cf-d-flex cf-justify-content-between">
				{(status === 'done' || status === 'need-reaction') ? (
					<Reaction status={reaction} id={id}/>
				) : <div/>}
				<div>
					{(status === 'done' || status === 'need-reaction') && (
						<Link to={`/detail/${recipeId}?n=${nServing}`}
						data-cy="history-item-text-done"
						className="cf-text-primary cf-pointer">
							Selesai (100%)
						</Link>
					)}
					{status === 'progress' && (
						<Link to={`/cooking/${id}`}
						data-cy="history-item-text-progress"
						className="cf-text-primary cf-pointer">
							Dalam Proses ({Math.round((nStepDone/nStep) * 100)}%)
						</Link>
					)}
				</div>
			</div>
		</div>
	)
});

let _debounce = null;

export default memo(() =>
{
	const dispatch = useDispatch();
	const routerHistory = useHistory();
	const {token} = useSelector(({auth}) => auth);

	useEffect(() => {
		dispatch(resetHistory());
	}, [dispatch]);

	useEffect(() => 
	{
		const onScroll = () => {
			const {documentElement: {scrollHeight, scrollTop}} = document;

			if (_debounce) {
				clearTimeout(_debounce);
				_debounce = null;
			}

			_debounce = setTimeout(() => {
				if ((scrollHeight - scrollTop) < 700) dispatch(loadMoreHistory());
			}, 200);
		};

		document.removeEventListener('scroll', onScroll);
		document.addEventListener('scroll', onScroll);

		if (token) {
			dispatch(getHistory());
		} else {
			routerHistory.push('/login');
		}

		return () => document.removeEventListener('scroll', onScroll);
	}, [token, dispatch, routerHistory]);

	const {data, loading} = useSelector(({history}) => history);

	return (
		<React.Fragment>
			<div className="cf-w-100 cf-mt-2">
				<MenuSorter isHistory/>
			</div>
			<div className="cf-w-100 cf-my-3">
				{data.map((r, i) => <HistoryItem key={i} data={r} cy={`history-item-${i}`}/>)}
				{loading && (
					<div className="cf-w-100 cf-text-center cf-my-3">
						<h5>Memuat data...</h5>
					</div>
				)}
				{!(data && data.length) && (
					<div className="cf-w-100 cf-text-center cf-mb-5">
						<span className="cf-menu-empty"/>
						<h4 className="cf-text-muted cf-fw-normal">Oops! Resep tidak ditemukan.</h4>
					</div>
				)}
			</div>
		</React.Fragment>
	)
});