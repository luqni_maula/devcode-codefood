import React, {memo, useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useHistory, useParams} from "react-router-dom";
import ReactionBadge from "@components/Menu/ReactionBadge";
import Api from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError, fetchError} from "@reduxActions/Common";
import {getCookingSteps} from "@reduxActions/Cooking";

export default memo(() =>
{
	const dispatch = useDispatch();
	const history = useHistory();
	const {location: {search}} = history;
	const q = new URLSearchParams(search);
	const [qty, setQty] = useState(q.get('n') && !isNaN(parseInt(q.get('n'))) ? parseInt(q.get('n')) : 1);
	const [loading, setLoading] = useState(false);
	const [data, setData] = useState(null);
	const {slug} = useParams();
	const {token} = useSelector(({auth}) => auth);

	const getDetailComposition = async () =>
	{
		try {
			setLoading(true);
			const {data: {success, message, data}} = await Api.get(RouteAccess.getApi('RECIPES_DETAIL', {id: slug}), {
				params: {
					nServing: qty || 1
				}
			});
			if (success) {
				setData(data);
			} else {
				dispatch(fetchError(message));
			}
		} catch(e) {
			dispatch(setError(e));
		} finally {
			setLoading(false);
		}
	}

	const [message, setMessage] = useState('');
	useEffect(() => {
		getDetailComposition();
		setMessage(qty > 0 ? '' : 'Jumlah porsi minimal adalah 1');
	}, [slug, qty]);

	const {
		name,
		image,
		nReactionLike,
		nReactionNeutral,
		nReactionDislike,
		ingredientsPerServing
	} = data ?? {};

	const [starting, setStarting] = useState(false);
	const startCooking = async () =>
	{
		if (token)
		{
			if (qty > 0)
			{
				setStarting(true);
				dispatch(getCookingSteps(slug, () => {
					setStarting(false);
					history.push('/cooking/'+slug+'?n='+qty);
				}));
			}
		} else {
			history.push('/login');
		}
	}

	return (
		<div className="cf-d-flex cf-justify-content-between cf-my-3">
			<div className="cf-detail-container cf-mr-4">
				<div className="cf-detail-image-container">
					<img alt="" className="cf-menu-image-large" src={image} data-cy="detail-image"/>
					<div className="cf-back-button">
						<Link to="/" 
						className="cf-btn cf-btn-default cf-bg-white cf-mr-3 cf-align-items-center"
						data-cy="button-back">
							<span className="cf-icon-back"/>
						</Link>
					</div>
				</div>
				<div className="cf-detail-body">
					<h2 className="cf-mt-0" data-cy="detail-text-title">{name}</h2>
					<div className="cf-d-inline-flex cf-justify-content-between">
						<ReactionBadge
						type="success"
						count={nReactionLike}
						margin={2}
						cy="detail-like"
						cyValue="detail-like-value"/>
						<ReactionBadge
						type="warning"
						count={nReactionNeutral}
						margin={2}
						cy="detail-neutral"
						cyValue="detail-neutral-value"/>
						<ReactionBadge
						type="danger"
						count={nReactionDislike}
						margin={2}
						cy="detail-dislike"
						cyValue="detail-dislike-value"/>
					</div>
				</div>
				<hr/>
				{loading && (
					<div className="cf-w-100 cf-py-3 cf-text-center">
						<h5>Memuat data...</h5>
					</div>
				)}
				{(ingredientsPerServing && ingredientsPerServing.length > 0) && (
					<div className="cf-detail-body">
						<h4 className="cf-mt-0" data-cy="detail-text-ingredients">Bahan-bahan</h4>
						<div data-cy="detail-text-recipe">
							{ingredientsPerServing.map(({item, unit, value}, i) => (
								<p key={i} className="cf-detail-composition cf-my-1"><b>{value} {unit}</b> {item}</p>
							))}
						</div>
					</div>
				)}
			</div>
			<div className="cf-detail-quantity" data-cy="form-portion">
				<div className="cf-quantity-body">
					<h4 className="cf-mt-0" data-cy="form-text-title-portion">Jumlah porsi yang dimasak:</h4>
					<div className="cf-d-flex cf-align-items-center cf-mb-3">
						<div data-cy="form-button-decrease-portion" 
						className={`cf-icon-circle-minus ${(!loading || !starting) && 'cf-pointer'} cf-mr-2`}
						onClick={() => (!loading && !starting) && setQty((v) => v >= 1 ? v - 1 : 0)}/>
						<div className="cf-mr-2">
							<input 
							data-cy="form-input-portion"
							type="number" 
							value={qty}
							disabled={loading || starting}
							onChange={({target: {value}}) => setQty(value >= 0 ? value : 0)}
							className="cf-qty-box cf-text-center"/>
						</div>
						<div data-cy="form-button-increase-portion" 
						className={`cf-icon-circle-plus ${(!loading || !starting) && 'cf-pointer'}`}
						onClick={() => (!loading && !starting) && setQty((v) => v + 1)}/>
					</div>
					<button
					data-cy="form-button-submit-portion"
					className="cf-btn cf-btn-primary cf-w-100" 
					disabled={qty < 1 || loading || starting}
					onClick={() => {
						if (qty > 0 && !loading && !starting) startCooking();
					}}>
						{starting ? 'Memproses...' : 'Mulai Memasak'}
					</button>
				</div>
				{message && (<div className="cf-mt-4 cf-error-message">
					<div className="cf-d-flex cf-justify-content-between cf-px-3 cf-py-3">
						<small>{message}</small>
						<small className="cf-error-message-ok"
						onClick={() => setMessage('')}>
							OK
						</small>
					</div>
				</div>)}
			</div>
		</div>
	)
});