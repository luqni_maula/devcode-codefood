import React, {memo, Suspense, lazy, useEffect} from "react";
import {useSelector} from "react-redux";
import {Route, Switch, useHistory} from "react-router-dom";
import FallbackContent from "@app/components/FallbackContent";

export default memo(({match}) => 
{
	const history = useHistory();
	const {token} = useSelector(({auth}) => auth);
	useEffect(() => {
		if (!token) history.push('/login');
	}, [token, history]);

	return (
		<Suspense fallback={<FallbackContent/>}>
		    <Switch>
		        <Route path={`${match.url}/:slug`} exact component={lazy(() => import('./Cooking'))}/>
		        <Route path={`${match.url}/:slug/rating`} component={lazy(() => import('./Rating'))}/>
		    </Switch>
		</Suspense>
	)
});