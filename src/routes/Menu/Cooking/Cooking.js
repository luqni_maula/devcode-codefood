import React, {memo, useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useParams, useHistory} from "react-router-dom";
import {getCookingData, markStepDone, getCookingSteps} from "@reduxActions/Cooking";

const CookingStepItem = memo(({id, description, done, ready, stepOrder, isOngoing, isLast, cy}) =>
{
	const dispatch = useDispatch();
	const history = useHistory();
	const {slug} =  useParams();

	let statusCls = '';
	let statusIconCls = '';

	if (isOngoing) {
		statusCls = 'cf-cook-step-item-ongoing';
	}

	if (done || isLast) {
		statusCls = 'cf-cook-step-item-done';
		statusIconCls = 'cf-cook-step-item-icon-done';
	}

	const [loading, setLoading] = useState(false);
	const markDone = async (callback) =>
	{
		setLoading(true);
		dispatch(markStepDone(slug, stepOrder, () => {
			setLoading(false);
			if (callback) callback();
		}));
	}

	return (
		<div className={`cf-w-100 cf-cook-step-item cf-pb-4 ${statusCls}`} data-cy={cy}>
			<div className={`cf-cook-step-item-icon ${statusIconCls}`}/>
			<div className="cf-ml-4 cf-pb-4 cf-cook-step-item-body">
				<h4 className={`cf-my-0 ${(!isOngoing && !done) && 'cf-text-muted'}`}
				data-cy="text-step-title">
					Step {stepOrder}
				</h4>
				<small className={`${(!isOngoing && !done) && 'cf-text-muted'}`}
				data-cy="text-step-description">
					{description}
				</small>
				{done && (
					<div className="cf-my-3 cf-align-items-center cf-text-success">
						<span className="cf-icon-check-success"/>
						<small className="cf-ml-1" data-cy="text-step-done">Selesai</small>
					</div>
				)}
				{isOngoing && (
					<React.Fragment>
						{isLast ? (
							<button className="cf-btn cf-btn-primary cf-my-3 cf-d-block"
							disabled={loading}
							data-cy="button-serve"
							onClick={() => markDone(() => {
								history.push(`/cooking/${slug}/rating`);
							})}>
								{loading ? 'Memproses...' : 'Sajikan Makanan'}
							</button>
						) : (
							<button className="cf-btn cf-btn-success cf-d-block cf-my-3 cf-w-175"
							disabled={loading}
							data-cy="button-step-done"
							onClick={() => markDone()}>
								{loading ? 'Memproses...' : 'Selesai'}
							</button>
						)}
					</React.Fragment>
				)}
			</div>
		</div>
	)
});

export default memo(() =>
{
	const dispatch = useDispatch();
	const {slug} = useParams();

	const {location: {search}, push} = useHistory();
	const {data, steps} = useSelector(({cooking}) => cooking);
	useEffect(() => {
		const q = new URLSearchParams(search);

		if (steps.length < 1)
		{
			if (q.get('n')) {
				dispatch(getCookingSteps(slug));
			} else {
				dispatch(getCookingData(slug));
			}
		}
	}, [dispatch, steps, slug, search]);

	const isOngoing = (index) =>
	{
		if ((steps?.length > 0))
		{
			const lastIndexProgress = steps.findIndex((r) => !r.done);
			return lastIndexProgress === index;
		}
		return false;
	}

	const {token} = useSelector(({auth}) => auth);
	useEffect(() => {
		if (!token) push('/login')
	}, [token, push]);

	const isLast = (index) => isOngoing(index) && index === (steps.length - 1);

	return (
		<div className="cf-cooking-container cf-my-3">
			<div className="cf-d-inline-flex cf-align-items-center">
				<Link to={'/detail/'+ (data?.recipeId ? data.recipeId : slug)}
				data-cy="button-back"
				className="cf-btn cf-btn-default cf-bg-white cf-mr-3 cf-align-items-center">
					<span className="cf-icon-back"/>
				</Link>
				<h2 className="cf-my-0" data-cy="text-title">Langkah Memasak</h2>
			</div>
			<div className="cf-mx-3 cf-mt-4">
				{(steps.length > 0) && steps.map((row, index) => (
					<CookingStepItem
					{...row}
					isOngoing={isOngoing(index)}
					key={index}
					isLast={isLast(index)}
					cy={`item-step-${index}`}/>
				))}
			</div>
		</div>
	)
});