import React, {memo, useState, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {Link, useParams, useHistory} from "react-router-dom";
import {getCookingData, saveReaction} from "@reduxActions/Cooking";

const reactionOptions = [
	{
		value: 'like',
		title: 'Yummy!',
		cls: 'cf-icon-good-taste',
		cy: 'button-like'
	},
	{
		value: 'neutral',
		title: 'Lumayan',
		cls: 'cf-icon-avg-taste',
		cy: 'button-neutral'
	},
	{
		value: 'dislike',
		title: 'Kurang Suka',
		cls: 'cf-icon-bad-taste',
		cy: 'button-dislike'
	},
];

export default memo(() =>
{
	const {slug} = useParams();
	const dispatch = useDispatch();
	const history = useHistory();
	const [reaction, setReaction] = useState(null);
	const handleReaction = (value) => setReaction(reaction === value ? null : value);
	const {data, loading} = useSelector(({cooking}) => cooking);

	useEffect(() => {
		dispatch(getCookingData(slug));
	}, [dispatch, slug]);

	const [submiting, setSubmiting] = useState(false);
	const submitReaction = () =>
	{
		if (reaction)
		{
			setSubmiting(true);
			dispatch(saveReaction(slug, reaction, () =>
			{
				setSubmiting(false);
			}));
		}
	}

	const {token} = useSelector(({auth}) => auth);
	useEffect(() => {
		if (!token) history.push('/login')
	}, [token, history]);

	return (
		<div className="cf-rating-container cf-my-3">
			{data?.reaction ? (
				<div className="cf-w-100 cf-text-center cf-my-5">
					<div className="cf-w-100 cf-text-center cf-my-5">
						<span className="cf-rating-thanks cf-mb-5" data-cy="image-thanks"/>
						<h2 className="cf-fw-normal cf-my-1" data-cy="text-description">
							Terimakasih telah memberikan<br/>penilaianmu!
						</h2>
					</div>
					<div className="cf-rating-reaction-container">
						<Link to="/" className="cf-btn cf-btn-primary cf-w-100" data-cy="button-home">
							Kembali Ke Beranda
						</Link>
					</div>
				</div>
			) : (
				<React.Fragment>
					<h3 className="cf-mt-0">Beri Penilaian</h3>
					<div className="cf-w-100 cf-text-center cf-my-5">
						<h4 className="cf-mt-0" data-cy="text-title">Yaay! Masakanmu sudah siap disajikan</h4>
						<div className="cf-w-100 cf-text-center cf-my-5">
							<span className="cf-foods-drinks" data-cy="image-rate"/>
							<h4 className="cf-fw-normal cf-my-1" data-cy="text-description">Suka dengan resep dari CodeFood?<br/>Bagaimana rasanya?</h4>
						</div>
						<div className="cf-rating-reaction-container">
							<div className="cf-d-flex cf-justify-content-between cf-mb-4">
								{reactionOptions.map(({value, title, cls, cy}, i) => (
									<div key={i} className={`cf-pointer cf-text-center`}
									onClick={() => handleReaction(value)} data-cy={cy}>
										<span className={`${cls}${value === reaction ? '-active' : ''}`}/>
										<h5 className={`cf-mt-2 cf-mb-0 ${value !== reaction && 'cf-text-muted'}`}>{title}</h5>
									</div>
								))}
							</div>
							<button
							data-cy="button-rate"
							className="cf-btn cf-btn-primary cf-w-100" 
							disabled={!reaction || loading || submiting}
							onClick={() => submitReaction()}>
								{submiting ? 'Menyimpan Penilaian...' : 'Berikan Penilaian'}
							</button>
						</div>
					</div>
				</React.Fragment>
			)}
		</div>
	)
});