export const priorities = [
	{
		title: 'Very High',
		value: 'very-high',
		styleName: 'todo-label-indicator todo-very-high',
	},
	{
		title: 'High',
		value: 'high',
		styleName: 'todo-label-indicator todo-high',
	},
	{
		title: 'Normal',
		value: 'normal',
		styleName: 'todo-label-indicator todo-normal',
	},
	{
		title: 'Low',
		value: 'low',
		styleName: 'todo-label-indicator todo-low',
	},
	{
		title: 'Very Low',
		value: 'very-low',
		styleName: 'todo-label-indicator todo-very-low',
	},
];

export const sorterOptions = [
	{
		name: 'Terbaru',
		value: null,
		cy: 'button-sort-latest'
	},
	{
		name: 'Urutkan A-Z',
		value: 'name_asc',
		cy: 'button-sort-az'
	},
	{
		name: 'Urutkan Z-A',
		value: 'name_desc',
		cy: 'button-sort-za'
	},
	{
		name: 'Urutkan Dari Paling Disukai',
		value: 'like_desc',
		cy: 'button-sort-favorite'
	},
];