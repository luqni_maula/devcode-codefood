import React, {memo, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux"
import {hideMessage} from "@reduxActions/Common";

export default memo(() =>
{
    const dispatch = useDispatch();
    const errorMessage = useSelector(({common}) => common.error);
    const infoMessage = useSelector(({common}) => common.message);

    const [showInfo, setShowInfo] = useState(false);
    useEffect(() => {
        if (infoMessage) setShowInfo(true);
    }, [infoMessage]);

    const [showError, setShowError] = useState(false);
    useEffect(() => {
        if (errorMessage) setShowError(true);
    }, [errorMessage]);

    useEffect(() => 
    {
        // handle modal close outside of modal content
        const handleModalVisibility = (e) => {
            const modalParent = document.querySelector('.cf-modal-info');
            if (modalParent && !modalParent.contains(e.target)) 
            {
                dispatch(hideMessage());
                setShowInfo(false);
            }
        };

        if (showInfo) {
            document.body.addEventListener('click', handleModalVisibility);
        } else {
            document.body.removeEventListener('click', handleModalVisibility);
        }

        return () => document.body.removeEventListener('click', handleModalVisibility);
    }, [showInfo, dispatch]);

    useEffect(() => 
    {
        // handle modal close outside of modal content
        const handleModalVisibility = (e) => {
            const modalParent = document.querySelector('.cf-modal-error');
            if (modalParent && !modalParent.contains(e.target)) 
            {
                dispatch(hideMessage());
                setShowError(false);
            }
        };

        if (showError) {
            document.body.addEventListener('click', handleModalVisibility);
        } else {
            document.body.removeEventListener('click', handleModalVisibility);
        }

        return () => document.body.removeEventListener('click', handleModalVisibility);
    }, [showError, dispatch]);

    return (
        <React.Fragment>
            {showInfo && (
                <div className="cf-modal-wrapper">
                    <div className="cf-modal-content cf-modal-info" data-cy="modal-information">
                        <div className="cf-d-inline-flex cf-justify-content-between cf-align-items-center cf-my-4">
                            <span className="cf-icon-info-circle cf-text-primary cf-mr-2 cf-fs-lg" data-cy="modal-information-icon"/>
                            <span data-cy="modal-information-title">{infoMessage}</span>
                        </div>
                    </div>
                </div>
            )}
            {showError && (
                <div className="cf-modal-wrapper">
                    <div className="cf-modal-content cf-modal-error" data-cy="modal-information">
                        <div className="cf-d-inline-flex cf-justify-content-between cf-align-items-center cf-my-4">
                            <span className="cf-icon-info-circle cf-text-danger cf-mr-2 cf-fs-lg" data-cy="modal-information-icon"/>
                            <span data-cy="modal-information-title">{errorMessage}</span>
                        </div>
                    </div>
                </div>
            )}
        </React.Fragment>
    );
});