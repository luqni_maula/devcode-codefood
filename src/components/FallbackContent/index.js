import React, {memo} from "react";

const FallbackContent = () => (
	<div className="cf-w-100 cf-h-100 cf-text-center cf-mt-5" style={{verticalAlign: 'middle'}}>
		<h3>Loading...</h3>
    </div>
);

export default memo(FallbackContent);