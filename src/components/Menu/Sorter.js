import React, {memo} from "react";
import {useSelector, useDispatch} from "react-redux";
import {setRecipesSorter} from "@reduxActions/Recipes";
import {setHistorySorter} from "@reduxActions/History";

export default memo(({isHistory}) =>
{
	const dispatch = useDispatch();
	const {sorters, selected_sorter, loading} = useSelector(({recipes}) => recipes);
	const {sorters: sortersHistory, sorter, loading: loadingHistory} = useSelector(({history}) => history);

	return (
		<div className="cf-d-inline-flex cf-align-items-center">
			<span className="cf-mr-2">Urutkan:</span>
			{isHistory ? (
				<React.Fragment>
					{sortersHistory.map(({name, value, cy}, index) => (
						<div key={index} data-cy={cy}
						className={`cf-menu-sorter ${(!loadingHistory && value !== sorter) && 'cf-pointer'} cf-mr-2 ${(value === sorter) && 'cf-menu-sorter-active'}`}
						onClick={() => {
							if (!loadingHistory && value !== sorter) dispatch(setHistorySorter(value));
						}}>
							{name}
						</div>
					))}
				</React.Fragment>
			) : (
				<React.Fragment>
					{sorters.map(({name, value, cy}, index) => (
						<div key={index} data-cy={cy}
						className={`cf-menu-sorter ${(!loading && value !== selected_sorter) && 'cf-pointer'} cf-mr-2 ${(value === selected_sorter) && 'cf-menu-sorter-active'}`}
						onClick={() => {
							if (!loading && value !== selected_sorter) dispatch(setRecipesSorter(value));
						}}>
							{name}
						</div>
					))}
				</React.Fragment>
			)}
		</div>
	)
});