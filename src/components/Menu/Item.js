import React, {memo} from "react";
import {Link} from "react-router-dom";
import ReactionBadge from "@components/Menu/ReactionBadge";

export default memo(({data, dataCy}) =>
{
	const {id, name, image, nReactionLike, nReactionNeutral, nReactionDislike, recipeCategory} = data;
	return (
		<div className="cf-menu">
			<Link to={`/detail/${id}`} data-cy={dataCy}>
				<img alt="" className="cf-menu-image" src={image} data-cy="list-item-image"/>
				<div className="cf-menu-body cf-position-relative">
					<h4 data-cy="list-item-text-title"
					className="cf-menu-title cf-my-0 cf-text-truncate-two-lines">
						{name}
					</h4>
					<h4 data-cy="list-item-text-category"
					className="cf-menu-subtitle cf-my-0">
						{recipeCategory?.name}
					</h4>
					<div className="cf-d-inline-flex cf-justify-content-between cf-mt-3 cf-position-absolute" style={{bottom: 0}}>
						<ReactionBadge
						type="success"
						count={nReactionLike}
						cy="list-item-like"
						cyValue="list-item-like-value"/>
						<ReactionBadge
						type="warning"
						count={nReactionNeutral}
						cy="list-item-neutral"
						cyValue="list-item-neutral-value"/>
						<ReactionBadge
						type="danger"
						count={nReactionDislike}
						cy="list-item-dislike"
						cyValue="list-item-dislike-value"/>
					</div>
				</div>
			</Link>
		</div>
	)
});