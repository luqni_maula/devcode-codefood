import React, {memo} from "react";

export default memo(({type, count, margin, cy, cyValue}) => {
	return (
		<div data-cy={cy} className={`cf-menu-reaction-badge cf-align-items-center cf-mr-${margin ? margin : 1}`}>
			<span className={`cf-icon-reaction-${type} cf-mr-1`}/>
			<span data-cy={cyValue}>{count}</span>
		</div>
	)
});